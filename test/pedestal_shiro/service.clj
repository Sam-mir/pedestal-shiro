(ns pedestal-shiro.service
  (:import [org.apache.shiro.web.mgt DefaultWebSecurityManager]
           [org.apache.shiro.authc UsernamePasswordToken])
  (:require [io.pedestal.http :as bootstrap]
            [io.pedestal.http.route :as route]
            [io.pedestal.log :as log]
            [io.pedestal.interceptor :refer [definterceptor]]
            [io.pedestal.http.body-params :as body-params]
            [io.pedestal.http.route.definition :refer [defroutes]]
            [io.pedestal.http.ring-middlewares :as middlewares]
            [ring.middleware.session.cookie :as cookie]
            [pedestal-shiro.core :as pedestal-shiro]
            [pocheshiro.core :as shiro]
            [ring.util.response :as ring-resp]))

(definterceptor session-interceptor
  (middlewares/session {:store (cookie/cookie-store)}))

(def users (atom {}))

(def bcrypted-passwords (shiro/bcrypt-passwords {}))

(defn register-user! [{:keys [username password roles permissions]}]
  (let [hashed-pwd (.encryptPassword bcrypted-passwords password)]
    (swap! users assoc username {:username username
                                 :roles (set roles)
                                 :permissions (set permissions)
                                 :password-hash hashed-pwd})))

(def test-realm (shiro/username-password-realm 
                 :passwords bcrypted-passwords
                 :get-authentication
                 (fn [token] 
                   (when-let [user (get @users (.getPrincipal token))]
                     {:principal (:username user)
                      :credentials (:password-hash user)}))
                 :get-authorization
                 (fn [principal]
                   (when-let [user (get @users (.getPrimaryPrincipal principal))]
                     (select-keys user [:roles :permissions])))))

(register-user! {:username "john"
                 :password "secret"
                 :roles #{:member :manager}
                 :permissions #{:write}})

(def security-manager-retriever
   (constantly (DefaultWebSecurityManager. test-realm)))

(defn secure-page [req] 
  (ring-resp/response "Secure"))

(defn secure-silent-page [req] 
  (ring-resp/response "Secure Silent"))

(defn home-page [req] 
  (ring-resp/response "Home"))

(defn login [req] 
  (ring-resp/response "Login"))

(defn do-login [req]
  (shiro/login! (UsernamePasswordToken. "john" "secret"))
  (ring-resp/response "Login Successful"))

(defn do-logout [req]
  (shiro/logout!)
  (ring-resp/response "Logout"))

(defn public-page [req] 
  (ring-resp/response "Public"))

(defn members-only [req]
  (ring-resp/response "Members Only"))

(defn members-with-write-only [req]
  (ring-resp/response "Members With Write Only"))

(defn access-denied [req]
  (ring-resp/response "Access Denied"))

(defroutes routes
  [[["/" {:get home-page}
     ;; Set default interceptors for /about and any other paths under /
     ^:interceptors [session-interceptor
                     (pedestal-shiro/security 
                      {:security-manager-retriever security-manager-retriever
                       :login-page :login :access-denied-page :access-denied})
                     (body-params/body-params) bootstrap/html-body]
     ["/public"   {:get public-page}]
     ["/secure"   {:get secure-page} 
      ^:interceptors [(pedestal-shiro/guard shiro/authenticated)]]
     ["/members-only" {:get members-only}
      ^:interceptors [(pedestal-shiro/guard (shiro/authorized {:roles #{:member}}))]]
     ["/members-with-write" {:get members-with-write-only}
      ^:interceptors [(pedestal-shiro/guard
                       (shiro/and* (shiro/authorized {:roles #{:member}})
                                   (shiro/authorized {:permissions #{:write}})))]]
     ["/logout"   {:get do-logout}]
     ["/do-login" {:get do-login}]
     ["/access-denied" {:get [:access-denied access-denied]}]
     ["/login"    {:get [:login login]}]]]])

;; Consumed by pedestal-shiro.server/create-server
;; See bootstrap/default-interceptors for additional options you can configure
(def service {:env :prod
              ;; You can bring your own non-default interceptors. Make
              ;; sure you include routing and set it up right for
              ;; dev-mode. If you do, many other keys for configuring
              ;; default interceptors will be ignored.
              ;; :bootstrap/interceptors []
              ::bootstrap/routes routes

              ;; Uncomment next line to enable CORS support, add
              ;; string(s) specifying scheme, host and port for
              ;; allowed source(s):
              ;;
              ;; "http://localhost:8080"
              ;;
              ;;::bootstrap/allowed-origins ["scheme://host:port"]

              ;; Root for resource interceptor that is available by default.
              ::bootstrap/resource-path "/public"

              ;; Either :jetty, :immutant or :tomcat (see comments in project.clj)
              ::bootstrap/type :jetty
              ;;::bootstrap/host "localhost"
              ::bootstrap/port 8080})
